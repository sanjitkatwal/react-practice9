import React, {Component} from 'react';
import './App.css';
import Greet from './components/1props/Greet';
import Welcome from './components/1props/Welcome'
import Message from './components/2state/Message'
import Counter from './components/2state/Counter'
import Destructuring from './components/1props/Destructuring';
import Destructuring2 from './components/1props/Destructuring2';
import Destructuring_in_class_compo from "./components/1props/Destructuring_in_class_compo";
import FunctionClick from "./components/3function/FunctionClick";
import ClassClick from "./components/ClassClick";
import EventBind from "./components/EventBind";
import ParentComponent from "./components/ParentComponent";
import Form from './components/Form'

class App extends Component {
  render(){
    return (
        <div className="App">
            <Greet name="Sanjit" heroName="Super Man">
                <p>This is Children Group.</p>
            </Greet>
            <Greet name="Durga" heroName="Iron Man">
                <button>Click Me</button>
            </Greet>
            <Greet name="Ranjit" heroName="Spider Man" />

            <Welcome name="Sujit" heroName="Super Man" />
            <Welcome name="Jivan" heroName="Super Man" />
            <Welcome name="Raju" heroName="Super Man" />
            <h1 color="red">--------------------State------------------</h1>
            <Message />
            <h1 color="red">--------------------setState------------------</h1>
            <Counter />
            <h1 color="red">--------------------Destructuring in functional parameter------------------</h1>
            <Destructuring name="Raju" heroName="Super Man" />
            <h1 color="red">--------------------Destructuring functional body------------------</h1>
            <Destructuring2 name="Raju" heroName="Super Man" />
            <h1 color="red">--------------------Destructuring Class Compo------------------</h1>
            <Destructuring_in_class_compo name="Raju" heroName="Super Man" />
            <h1 color="red">--------------------Function Click event------------------</h1>
            <FunctionClick />
            <h1>--------------------Class Click event------------------</h1>
            <ClassClick />
            <h1>--------------------Bind Event Handler------------------</h1>
            <EventBind />

            <h1>--------------------Method as Props--------------------</h1>
            <ParentComponent />
            <h1>--------------------Form Handling--------------------</h1>
            <Form />
        </div>
    );
  }
}

export default App;
