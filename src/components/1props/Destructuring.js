import React, {Component} from 'react'

const Destructuring = ({name, heroName}) => {
    return(
        <div>
            <p>Destructuring in functional parameter</p>
            <h2>Hello {name} A.K.A. {heroName}</h2>

        </div>

    )
}

export default Destructuring