import React, {Component} from 'react'

const Destructuring1 = props => {
    const {name, heroName} = props
    return(
        <div>
            <p>Destructuring in functional body</p>
            <h2>Hello {name} A.K.A. {heroName}</h2>

        </div>

    )
}

export default Destructuring1