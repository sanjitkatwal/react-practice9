import React, {Component} from 'react'

class Destructuring_in_class_compo extends Component{
    render() {
        const {name, heroName} = this.props
        return(
            <div>
            {/*<p>Welcom {this.props.name} a.k.a {this.props.heroName}</p>*/}
            {/*//for the state use - const {state1, state2} = this.state*/}
            <p>Welcom {name} a.k.a {heroName}</p>
            </div>
        )
    }
}

export default Destructuring_in_class_compo