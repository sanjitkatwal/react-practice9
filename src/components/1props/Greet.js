import React, {Component} from 'react'

const Greet = props => {
    return(
        <div>
            <h2>Hello {props.name} A.K.A. {props.heroName}</h2>
            {props.children}
        </div>

    )
}

export default Greet