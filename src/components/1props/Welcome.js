import React, {Component} from "react";

class Welcome extends Component{
    render() {
        return(
            <div>
                <h2>This is welcome page</h2>
                <p>Welcom {this.props.name} a.k.a {this.props.heroName}</p>
            </div>
        )
    }
}


export  default Welcome;