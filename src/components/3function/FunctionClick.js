import  React from 'react'

function FunctionClick(){
    function clickHandler(){
        alert('Click From Functional Component')
    }
    return(
        <div>
        <button onClick={clickHandler}>Click Me</button>
        </div>
    )
}


export  default FunctionClick