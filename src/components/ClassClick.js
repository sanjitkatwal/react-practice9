import  React, {Component} from 'react'

class  ClassClick extends Component{
    clickHandler(){
        alert('click the button from Class Component')
    }
    render() {
        return(
            <div>
                <button onClick={this.clickHandler}>Class Based Click Me</button>
            </div>
        )
    }
}


export  default ClassClick